// Array Methods - javascript has built in functions and methods for arrays. This allows us to manipulate and access array items.

//Mutator Methods

/*
	- Mutator methods are functions that "mutate" or change an array after they're created.
	- These methods manipulate the original array performing various task such as adding and removing elements. 

*/


//push()
/*
	- Adds an element in the end of an array And return the array's lenght. 

	Syntax:
		ArrayName.push();
*/

let fruits = ['apple', 'orange', 'kiwi','dragon fruit'];

console.log('Current Array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from psuh method: ');
console.log(fruits);

//Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push methods');
console.log(fruits);

//pop()

/*
	- Removes the last element in an array AND returns the remove elemets.
	- Syntax:
		arrayName.pop();

*/

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log('Mutated array from pop methods');
console.log(fruits);


//unshift()
/*
	- Adds one or more elemnts at the beginning of an array
	SYNTAX:
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', elementA');

*/

fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method");
console.log(fruits);

//shift();

/*
	-Removes an elements at the beginning of array and Retuns the removed element();
	Syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);

//splice()

/*
	- Simultaneusly removes elements from a specified index number and adds element
	SYNTAX:
		arrayName.splice(startingINdex,deleteCount,elementsToBEAdded);
 */


fruits.splice(1, 2, 'lime', 'Cherry');
console.log('Mutated array from splice');
console.log(fruits);


//Sort()

/*
	- Rearrange the array elements in alphanumeric orde.
	SYNTAX
		arrayName.sort();
*/

const randomThings = ['cat', 'boy', 'apps', 'zoo']

randomThings.sort();
console.log('Mutated array from sort method:');
console.log(randomThings);


//reverse();

/*
	-Reverse the order of array elements 
	SYNTAX
		arrayName.reverse();
*/

randomThings.reverse();
console.log('Mutated array from reverse method');
console.log(randomThings);


// Non -mutator Methods

/*
	- Non - mutator methods are functions that do not modify or change an array after they're created.
	- These methods do not manipulatrray performing varios task such as returning elemnts from an array and combining array and printing the output.
*/

//indexOf();
/*
	-Returning the index number of the first matching element found in an array.
	-if no match was found, the result will be 1
	- The search process will be done from first eleement proceeding to the elements.
	SYNTAX:
		arrayName.indexOFf(searchValue);

*/

let countries = ['US','PH','CAN','SG','TH','BR','FR','DE'];

let firstIndex = countries.indexOf('CAN');
console.log('Result of IndexOf method: ' + firstIndex);

//slice()
/*
 - Portion/slices elements from an array AND returns a new array.
 SYNTAX
 	arrayName.slice(startingIndex);
 	arrayName.slice(startingIndex,endingIndex);
*/

// Slicing of elements from a specific index to the first element

let slicedArrayA = countries.slice(4);
console.log('Result from slice methods: ');
console.log(slicedArrayA);
console.log(countries);

//Slicing off elements from a specified index to another index
//NOTE: the last elemets is not included. 
let sliceArrayB = countries.slice(1, 3);
console.log('Result from slice method:');
console.log(sliceArrayB);


//Slicing off elements starting from the last elements of an array
let slicedArrayC = countries.slice(-3);
console.log('Result from slice method: ');
console.log(slicedArrayC);

//forEach();

/*
	- Similar to a for loop that iterates on each array element.
	- For each item in the array, the anonymouse function passed in forEach() method will be run 
	- arrayName -plural for good practice
	- parameter -singular
	-NOTE: it must have function.
	SYNTAX:
		arrayName.forEach(function(indivElement)){
			statement
		}
*/

countries.forEach(function(country) {
	console.log(country);
})

//includes()
/*
	- includes() method checks if the arguments passed can be found in the array.
	-it returns a boolean which can be saved in a variable
		- return true if the argument is found in the array
		- return false if it is not

		SYNTAX:
			arrayName.include(<argumentToFind>)

*/
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];


let productFound1 = products.includes("Mouse");

        console.log(productFound1);//returns true

        let productFound2 = products.includes("Headset");

        console.log(productFound2);//returns false
